<?php

use Illuminate\Database\Seeder;

class MiscTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Misc::class,10)->create();
    }
}
