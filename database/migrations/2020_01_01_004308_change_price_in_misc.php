<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceInMisc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
                $table->renameColumn('price', 'wholesale_price');
                $table->float('retail_price',6,2);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
                $table->renameColumn('wholesale_price', 'price');
                $table->dropColumn('retail_price');
            });
        }
    }
}
