<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMiscs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
              if (Schema::hasColumn('miscs', 'misc_name')) {
                    $table->renameColumn('misc_name', 'size');
              } 
              if (Schema::hasColumn('miscs', 'size')) {
                  $table->float('price')->after('misc_name');
              }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
