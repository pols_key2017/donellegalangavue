<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products')) {

            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->string('product_name');
                $table->string('sku');
                $table->string('product_code');
                $table->integer('quantity');
                $table->integer('brand_id')->unsigned()->index()->nullable();
                $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
                $table->integer('product_category_id')->unsigned()->index()->nullable();
                $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('cascade');
                $table->dateTime('created_at');
                $table->dateTime('updated_at');
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
