<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierIdToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {

            Schema::table('products', function (Blueprint $table) {
              if (!Schema::hasColumn('products', 'supplier_id')) {
                    $table->integer('supplier_id')->unsigned()->index()->nullable();
                    $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
              }
              
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
          if (Schema::hasColumn('products', 'supplier_id')) {
               $table->dropForeign(['supplier_id']); 
               $table->dropColumn('supplier_id');
          }
        });
    }
}
