<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MiscAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('misc')) {

            Schema::create('misc', function (Blueprint $table) {
                $table->increments('id');
                $table->string('misc_name');
                $table->string('value');
                $table->dateTime('created_at');
                $table->dateTime('updated_at');
            });
            
        }

        if (!Schema::hasTable('product_misc')) {

            Schema::create('product_misc', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id');
                $table->integer('misc_id');
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
