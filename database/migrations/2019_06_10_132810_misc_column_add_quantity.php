<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MiscColumnAddQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
              if (!Schema::hasColumn('miscs', 'quantity')) {
                   $table->string('quantity')->after('price');
              }
            });
        }

        if (Schema::hasTable('products')) {
            Schema::table('products', function (Blueprint $table) {
              if (Schema::hasColumn('products', 'quantity')) {
                   $table->dropColumn('quantity');
              }
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
              if (Schema::hasColumn('miscs', 'quantity')) {
                  $table->dropColumn('quantity');
              }
            });
        }

        if (Schema::hasTable('products')) {
            Schema::table('products', function (Blueprint $table) {
              if (!Schema::hasColumn('products', 'quantity')) {
                   $table->integer('quantity')->after('product_code');
              }
            });
        }
    }
}
