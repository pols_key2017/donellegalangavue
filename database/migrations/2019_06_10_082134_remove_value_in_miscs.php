<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveValueInMiscs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
              if (Schema::hasColumn('miscs', 'value')) {
                   $table->dropColumn('value');
              }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
              if (!Schema::hasColumn('miscs', 'value')) {
                   $table->string('value')->after('price');
              }
            });
        }
    }
}
