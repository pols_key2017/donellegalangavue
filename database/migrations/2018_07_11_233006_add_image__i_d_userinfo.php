<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageIDUserinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('user_info')) {
            Schema::table('user_info', function (Blueprint $table) {
              if (!Schema::hasColumn('user_info', 'image_id')) {
                    $table->integer('image_id')->after('user_id');
              } 
            });
          }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function($table) {
           $table->dropColumn('image_id');
       });
    }
}
