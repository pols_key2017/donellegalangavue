<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSkuMisc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('miscs')) {

            Schema::table('miscs', function (Blueprint $table) {
              if (!Schema::hasColumn('miscs', 'sku')) {
                   $table->string('sku')->after('id');
              }
              if(Schema::hasColumn('miscs', 'size')){
                  $table->dropColumn('size');
              }
              if (!Schema::hasColumn('miscs', 'size_id')) {
                   $table->integer('size_id')->unsigned()->index()->nullable()->after('id');
                   $table->foreign('size_id')->references('id')->on('sizes')->onDelete('cascade');
              }

              if (!Schema::hasColumn('miscs', 'product_id')) {
                   $table->integer('product_id')->unsigned()->index()->nullable()->after('id');
                   $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
              }
            });
            
        }

        if (Schema::hasTable('products')) {

            Schema::table('products', function (Blueprint $table) {
              if (Schema::hasColumn('products', 'sku')) {
                   $table->dropColumn('sku');
              }
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('miscs')) {
            Schema::table('miscs', function (Blueprint $table) {
                if (Schema::hasColumn('miscs', 'sku')) {
                    $table->dropColumn('sku');
                }
                if(!Schema::hasColumn('miscs', 'size')){
                  $table->string('size')->after('id');
                }
                if (Schema::hasColumn('miscs', 'size_id')) {
                   $table->dropForeign(['size_id']);
                   $table->dropColumn('size_id');
                }
                if (Schema::hasColumn('miscs', 'product_id')) {
                   //Schema::disableForeignKeyConstraints();
                   $table->dropForeign(['product_id']);
                   $table->dropColumn('product_id');
                   //Schema::enableForeignKeyConstraints();
                }
            });
        }
        if (Schema::hasTable('products')) {
            Schema::table('products', function (Blueprint $table) {
                if (!Schema::hasColumn('products', 'sku')) {
                   $table->string('sku')->after('product_name');
              }
            });
        }
    }
}
