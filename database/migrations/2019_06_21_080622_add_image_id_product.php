<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageIdProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {

            Schema::table('products', function (Blueprint $table) {
              if (!Schema::hasColumn('products', 'image_id')) {
                    $table->bigInteger('image_id')->unsigned()->index()->nullable();
                    $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
              }
              
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
          if (Schema::hasColumn('products', 'image_id')) {
               $table->dropColumn('image_id');
          }
        });
    }
}
