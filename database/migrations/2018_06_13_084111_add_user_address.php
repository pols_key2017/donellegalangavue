<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            if (Schema::hasColumn('user_info', 'address')) {
                $table->renameColumn('address', 'street');
            }
            if (!Schema::hasColumn('user_info', 'phone')) {
                $table->string('city')->after('phone');
            }
            if (!Schema::hasColumn('user_info', 'city')) {
                $table->string('state')->after('city');
            }
            if (!Schema::hasColumn('user_info', 'country')) {
                $table->string('country')->after('state');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
