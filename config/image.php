<?php

return [
    'imagePath' => array(
	    	config('app.url').'/images/blog/',
	    	public_path() . '/images/blog/',
    	),
    'projectImgPath' => array(
    		config('app.url').'/images/project/',
	    	public_path() . '/images/project/',
    ),
    'profileImgPath' => array(
    		config('app.url').'/images/profile/',
    		public_path() . '/images/profile/',
    ),
    'productImgPath' => array(
            config('app.url').'/images/product/',
            public_path() . '/images/product/',
    ),
];
