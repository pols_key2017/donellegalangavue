<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Library\Imagelib;

use ImageCL;

use \App\blog as Blog; 
use \App\tag as Tag;
use \App\image as Image;
use Carbon\Carbon;
use \App\User;
use Auth;
use View;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminBlogController extends Controller
{

    protected $imageLib;
 
    public function __construct(Imagelib $Imagelib){
        $this->imageLib = $Imagelib;
    }

	public function single($id = null){

        if( $id > 0 ){
            $tags = \App\Blog::find($id)->tags()->get();
            $tagsArray = array();
            foreach($tags as $tag){
                $tagsArray[] = $tag->id;
            }
            $blog = \App\Blog::find($id);
            $featured_image = \App\Blog::find($id)->featuredImage()->first();
            $imagePath = config('image.imagePath');
            return response()->json(array(
                'blog' => $blog,
                'featuredImage' => !empty($featured_image) ? $featured_image : '',
                'imagePath' => $imagePath[0],
                'tagsSelected' => $tagsArray,
                'tags' => \App\Tag::all()
                ));
        }
    }

    public function bloginit(){
        return \App\Blog::where('status','!=','deleted')->get();
    }
    
    public function index(){
    	$data = \App\Blog::all();
        //$user = User::find(1);
        //$user->assignRole('super admin');
        /*$role = Role::create(['name' => 'super admin']);

        $permission = Permission::create(['name' => 'edit settings']);
        $role->givePermissionTo($permission);
        $permission->assignRole($role);*/
    	return View::make('backend.blog.blog',['datas' => $data]);
    }

    public function populateBlog(Request $request){
        $blogs = \App\Blog::orderBy('created_at', 'asc')->paginate(10);
        $data = array();
        $i = 0;
        foreach( $blogs as $key => $blog ){
            
            $data[] = array(
                'id' => $blog->id,
                'title' => $blog->title,
                'content' => $blog->content,
                'status' => $blog->status,
                'created_at' => !empty($blog->created_at) ? Carbon::parse($blog->created_at)->format('m/d/y') : '',
                'updated_at' => !empty($blog->updated_at) ? Carbon::parse($blog->updated_at)->format('m/d/y') : '',
                'user_id' => $blog->user_id,
                'tags' => \App\Blog::find($blog->id)->tags()->get(),
                'featured_image' => \App\Blog::findOrFail($blog->id)->featuredImage,
                );
            $i++;
        }
        $path = config('image.imagePath');
        $dataPagination = array(
               "total" => $blogs->total(),
               "per_page" => $blogs->perPage(),
               "current_page" => $blogs->currentPage(),
               "last_page" => $blogs->lastPage(),
               "first_page_url" => $blogs->firstItem(),
               "last_page_url" => $blogs->lastPage(),
               "next_page_url" => $blogs->nextPageUrl(),
               "prev_page_url" => $blogs->previousPageUrl(),
               "path" => $blogs->total(),
               "from" => $blogs->firstItem(),
               "to"=> $blogs->lastItem(),
               "path" => $path[0],
               "data" => $data,
            );

        

        return response()->json($dataPagination);
    }

    public function getByID($id){
        return response()->json(\App\Blog::find($id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request){
        
        $blog =  new \App\Blog;
        $blog->title = $request->input('title');
        $blog->content = $request->input('content');
        $blog->status = $request->input('status');
        $blog->tags = '';

        $blog->created_at = Carbon::now();
        $blog->user_id = Auth::id();
        $fileName = strip_tags($request->input('fileName'));
        $id_rand = mt_rand(10,100);
        $fileNameEnc = $this->imageLib->encode($id_rand,$fileName);
        
        $dataArray = array(
                'cropper_data' => $request->input('cropData'),
                'photoUrl' => $request->input('photo_data_url')
            );

        $blog->save(); 
        if( count($request->input('photo_data_url')) > 0 ){
            $extName = $this->imageProcess($dataArray,$fileNameEnc);
            $this->imageSaveInfo($blog,$fileNameEnc,$id_rand,$extName);
        }
        if( count($request->input('tags')) > 0) 
            $blog->tags()->attach($request->input('tags')); 

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return \App\Blog::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id){
        
        $blog = \App\Blog::find($id);
        $blog->title = $request->input('title');
        $blog->content = $request->input('content');
        $blog->status = $request->input('status');
        //$blog->update

        if( count($request->input('tags')) > 0) // sync removes the old data on the pivot table
            $blog->tags()->sync($request->input('tags')); // you can only use sync if its not NULL

        
        $fileName = strip_tags($request->input('fileName'));
        $fileNameEnc = $this->imageLib->encode($id,$fileName);

        $dataArray = array(
                'cropper_data' => $request->input('cropData'),
                'photoUrl' => $request->input('photo_data_url')
            );
        if( strlen($request->input('photo_data_url')) > 0 ){
            $extName = $this->imageProcess($dataArray,$fileNameEnc);

            $this->imageSaveInfo($blog,$fileNameEnc,$id,$extName);
        }

        $isSave = $blog->save();
        if( $isSave )
            return "Sucess updating blog #" . $blog->id; 
    }

    public function imageSaveInfo($blog,$fileNameEnc,$id,$extName = null){
        $img = $blog->featuredImage();
        if( $img->get()->count() <= 0 ){
            $params['name'] = $fileNameEnc;
            $params['updated_at'] = Carbon::now();
            $params['portion'] = 'featured';
            $params['ext_name'] = $extName;
            $image = new \App\Image($params);
            $img->save($image);
        }else{
            $img = $blog->featuredImage;
            $images = \App\Image::find($img->id);
            $images->updated_at = Carbon::now();
            $images->name = $fileNameEnc;
            $images->ext_name = $extName;
            $images->save();
            
        }
        
    }

    public function imageProcess( $array,$fileNameEnc ){
        // process image

        $blogImagePath = config('image.imagePath');
        $imgExt = '';
        if(strlen($array['photoUrl']) > 0){
            $img = ImageCL::make($array['photoUrl']);

            $imgExt = $this->imageLib->getFileExtension($img->mime());


                if( count($array['cropper_data']) > 0 ){
                    $img->crop(intval($array['cropper_data']['width']), intval($array['cropper_data']['height']), intval($array['cropper_data']['x']), intval($array['cropper_data']['y']))
                    ->resize(256, 256,
                        function ($constraint) {
                            $constraint->aspectRatio();
                        })
                    ->save($blogImagePath[1] . $fileNameEnc . '@avatar.' . $imgExt);
                    
                }

            $img->resize(520, 400,
                function ($constraint) {
                    $constraint->aspectRatio();
                })->save($blogImagePath[1] . $fileNameEnc . '.' . $imgExt);
            $img->destroy();
        }

        return $imgExt;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id){
        $blog = Blog::find( $id);
        $blog->status = 'deleted';
        $blog->save();
        return "Successfully deleted blog #" . $blog->id;
    }

    public function populateTags(){
        return response()->json(array(
            'tags' => \App\Tag::all()
            ));
    }

}
