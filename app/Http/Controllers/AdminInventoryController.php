<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use \App\Supplier;
use \App\ProductCategory;
use \App\Brand;
use \App\Size;
use \App\Product;
use \App\Misc;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use \App\Library\Imagelib;

class AdminInventoryController extends Controller
{

    protected $imageLib;
 
    public function __construct(Imagelib $Imagelib){
        $this->imageLib = $Imagelib;
    }

    public function test(){
        $data = array();
        $products = Product::paginate(10);
        foreach($products as $product){
            $prod = Product::findOrFail($product->id)->first();
            $data[] = array(
                'id' => $product->id,
                'productName' => $product->product_name,
                'misc' => $product->misc,
                'imageData' => $prod->image

            );
            foreach($product->misc as $misc){
                if(count($misc->size) > 0){
                   
                }
                
            }
        }
    }

    public function sizesShowAll(){
        return response()->json(array(
            'sizes' => \App\Size::all()
            ));
    }

    public function showProductDefaultAttr(){
        return response()->json(array(
            'sizes' => \App\Size::all(),
            'categories' => \App\ProductCategory::all(),
            'brands' => \App\Brand::all(),
            'suppliers' => \App\Supplier::where('active',1)->get(),
            ));
    }

    public function store(Request $request){
    	 $supplier =  new Supplier;
        $supplier->supplier_name = $request->input('supplier_name');
        $supplier->address = $request->input('address');
        $supplier->active = $request->input('status');

        $supplier->created_at = Carbon::now();

        $supplier->save();
    }

    public function addCategory(Request $request){
        $category =  new ProductCategory;
        $category->category = $request->input('category');
        $category->created_at = Carbon::now();

        $category->save();
    }

    public function addBrand(Request $request){
        $brand =  new Brand;
        $brand->brand = $request->input('brand');
        $brand->created_at = Carbon::now();

        $brand->save();
    }

    public function addProduct(Request $request){
        $product = new Product;

        $productImagePath = config('image.productImgPath');

        $dataArray = array(
              'cropper_data' => $request->input('cropData'),
              'photoUrl' => $request->input('photo_data_url'),
              'imagePath' => $productImagePath[1],
              'fileName' => $request->input('fileName'),
              'id' => mt_rand(10,100)
          );

        $fileName = strip_tags($request->input('fileName'));
        $id_rand = mt_rand(10,100);
        $fileNameEnc = $this->imageLib->encode($id_rand,$fileName);

        $product->product_name = $request->input('product');
        $product->product_code = $request->input('product_code');
        $product->brand_id = $request->input('brand_id');
        $product->supplier_id = $request->input('supplier_id');
        $product->product_category_id = $request->input('category_id');

        if( count($request->input('photo_data_url')) > 0 ){
            $file = $this->imageLib->processImage($dataArray);
            $params = array(
                'encodedName' => $file['fileName'],
                'portion' => 'project',
                'extensionName' => $file['extensionName']
            );
            //$id = $this->imageSaveInfo($blog,$fileNameEnc,$id_rand,$extName);
            $id = $this->imageLib->imageSaveInfo($params);
            $product->image_id = $id;
        }

        $product->save();
        //$product_id = $product->id;
        foreach( $request->input('miscs') as $misc ){
            $miscs = new Misc;
            $miscs->wholesale_price = $misc['wholesale_price'];
            $miscs->retail_price = $misc['wholesale_price'] * 2;
            $miscs->size_id = $misc['size_id'];
            $miscs->quantity = $misc['quantity'];
            $miscs->sku = $misc['sku'];
            $product->misc()->save($miscs);
            // Other way around but not working
            /*$params = array(
                "price" => $misc['price'],
                'quantity' => $misc['quantity'],
                "size_id" => $misc['size'],
                "sku" => $misc['sku']
                );
            $miscs[] = new Misc($params);*/
        }
        //$product->misc()->push($miscs);
    }

    public function populateSupplier(Request $request){
    	$suppliers = Supplier::orderBy('created_at', 'desc')->paginate(10);
        $data = array();

        foreach( $suppliers as $key => $supplier ){
            
            $data[] = array(
                'id' => $supplier->id,
                'supplier_name' => $supplier->supplier_name,
                'address' => $supplier->address,
                'status' => $supplier->active,
                'created_at' => !empty($supplier->created_at) ? Carbon::parse($supplier->created_at)->format('m/d/y') : '',
                );
        }
        return $this->dataPagination($suppliers,$data);
    }

    public function populateStock(){
        $products = Product::orderBy('created_at', 'desc')->paginate(10);
        $data = array();
        foreach($products as $key => $product ){
            $miscData = array();
            foreach($product->misc()->get() as $misc){

            }
            $data[] = array(
                'id' => $product->id,
                'product_name' => $product->product_name,
                'product_code' => $product->product_code,
                'product_category' => $product->category->category,
                'brand_id' => $product->brand_id,
                'category_id' => $product->product_category_id,
                'created_at' => !empty($product->created_at) ? Carbon::parse($product->created_at)->format('m/d/y') : '',
                'miscs' => $product->misc()->get()
                );
        }
        return $this->dataPagination($products,$data);
    }

    public function populateBrand(Request $request){
        $brands = Brand::orderBy('created_at', 'desc')->paginate(10);
        $data = array();

        foreach( $brands as $key => $brand ){
            
            $data[] = array(
                'id' => $brand->id,
                'brand' => $brand->brand,
                'created_at' => !empty($brand->created_at) ? Carbon::parse($brand->created_at)->format('m/d/y') : '',
                );
        }

        return $this->dataPagination($brands,$data);
    }

    public function populateCategories(Request $request){
        $productCategories = ProductCategory::orderBy('created_at', 'desc')->paginate(10);
        $data = array();

        foreach( $productCategories as $key => $productCategory ){
            
            $data[] = array(
                'id' => $productCategory->id,
                'category' => $productCategory->category,
                'created_at' => !empty($productCategory->created_at) ? Carbon::parse($productCategory->created_at)->format('m/d/y') : '',
                );
        }

        return $this->dataPagination($productCategories,$data);
    }

    public function single($id = NULL){
        if( $id > 0 ){
          $supplier = Supplier::where('id',(int) $id)->first();
          return response()->json(array(
                'supplier'=> $supplier,
                ));
        }
    }

    public function singleProduct($id = NULL){
         if( $id > 0 ){
          $product = Product::where('id',(int) $id)->first();
          $productImage = config('image.productImgPath');
          //$imagePath = $productImage[0].
          $imagePath = '';
          if($product->image != null){
            $imagePath = $productImage[0].$product->image->image_path;
          }
          return response()->json(array(
                'product'=> ['product' => $product, 'miscs' => $product->misc()->get(),'productImage' => $imagePath],
                'sizes' => \App\Size::all(),
                'categories' => \App\ProductCategory::all(),
                'brands' => \App\Brand::all(),
                'suppliers' => \App\Supplier::where('active',1)->get(),
                ));
        }
    }

    public function searchAllProduct(){
        $data = array();
        $products = Product::paginate(10);
        $productImagePath = config('image.productImgPath');
        foreach($products as $product){
            $prod = Product::findOrFail($product->id)->first();
            $imagePath = '';
            if($prod->image != null){
                $imagePath = $productImagePath[0].$product->image->image_path;
            }
            $data[] = array(
                'id' => $product->id,
                'productName' => $product->product_name,
                'imageUrl' =>$imagePath,
                'misc' => $product->misc,
                'imageData' => $prod->image

            );
        }

        $path = config('image.imagePath');
        $dataPagination = array(
               "total" => $products->total(),
               "per_page" => $products->perPage(),
               "current_page" => $products->currentPage(),
               "last_page" => $products->lastPage(),
               "first_page_url" => $products->firstItem(),
               "last_page_url" => $products->lastPage(),
               "next_page_url" => $products->nextPageUrl(),
               "prev_page_url" => $products->previousPageUrl(),
               "path" => $products->total(),
               "from" => $products->firstItem(),
               "to"=> $products->lastItem(),
               "path" => $path[0],
               "data" => $data,
            );
         
        return response()->json($dataPagination);
        
    }

    public function searchProduct( $productcode ){
        $data = array();
        $products = Product::where(function($query) use ($productcode) {
            $query->where('product_name', 'LIKE', '%'. $productcode. '%');
            $query->orWhere('product_code','LIKE', '%'. $productcode. '%');
        })->paginate(10);
        $productImagePath = config('image.productImgPath');
        foreach($products as $product){
            $prod = Product::findOrFail($product->id)->first();
            $imagePath = '';
            if($prod->image != null){
                $imagePath = $productImagePath[0].$product->image->image_path;
            }
            $data[] = array(
                'id' => $product->id,
                'productName' => $product->product_name,
                'imageUrl' =>$imagePath,
                'misc' => $product->misc,
                'imageData' => $prod->image

            );
        }

        $path = config('image.imagePath');
        $dataPagination = array(
               "total" => $products->total(),
               "per_page" => $products->perPage(),
               "current_page" => $products->currentPage(),
               "last_page" => $products->lastPage(),
               "first_page_url" => $products->firstItem(),
               "last_page_url" => $products->lastPage(),
               "next_page_url" => $products->nextPageUrl(),
               "prev_page_url" => $products->previousPageUrl(),
               "path" => $products->total(),
               "from" => $products->firstItem(),
               "to"=> $products->lastItem(),
               "path" => $path[0],
               "data" => $data,
            );
         
        return response()->json($dataPagination);
    }

    public function singleCategory($id = NULL){
      if( $id > 0 ){
          $categories = ProductCategory::where('id',(int) $id)->first();
          return response()->json(array(
                'categories'=> $categories,
                ));
        }
    }

    public function singleBrand($id = NULL){
      if( $id > 0 ){
          $brands = Brand::where('id',(int) $id)->first();
          return response()->json(array(
                'brands'=> $brands,
                ));
        }
    }

    public function update(Request $request, $id){
        $supplier = Supplier::find($id);
        $supplier->supplier_name = $request->input('supplier_name');
        $supplier->address = $request->input('address');
        $supplier->active = $request->input('status');
        $supplier->updated_at = Carbon::now();
        $isSave = $supplier->save();
        if( $isSave )
            return "Success updating Supplier #" . $supplier->id; 
    }

    public function updateCategory(Request $request, $id){
        $category = ProductCategory::find($id);
        $category->category = $request->input('category');
        $category->updated_at = Carbon::now();
        $isSave = $category->save();
        if( $isSave )
            return "Success updating Categories #" . $category->id; 
    }

    public function updateBrand(Request $request, $id){
        $brand = Brand::find($id);
        $brand->brand = $request->input('brand');
        $brand->updated_at = Carbon::now();
        $isSave = $brand->save();
        if( $isSave )
            return "Success updating Brand #" . $brand->id; 
    }

    public function updateProduct(Request $request, $id){
        $product = Product::find($id);

        $productImagePath = config('image.productImgPath');

        $dataArray = array(
              'cropper_data' => $request->input('cropData'),
              'photoUrl' => $request->input('photo_data_url'),
              'imagePath' => $productImagePath[1],
              'fileName' => $request->input('fileName'),
              'id' => mt_rand(10,100)
          );

        $fileName = strip_tags($request->input('fileName'));
        $id_rand = mt_rand(10,100);
        $fileNameEnc = $this->imageLib->encode($id_rand,$fileName);

        $product->product_name = $request->input('product');
        $product->product_code = $request->input('product_code');
        $product->brand_id = $request->input('brand_id');
        $product->supplier_id = $request->input('supplier_id');
        $product->product_category_id = $request->input('category_id');

        if( count($request->input('photo_data_url')) > 0 ){
            $file = $this->imageLib->processImage($dataArray);
            $params = array(
                'encodedName' => $file['fileName'],
                'portion' => 'project',
                'extensionName' => $file['extensionName']
            );
            //$id = $this->imageSaveInfo($blog,$fileNameEnc,$id_rand,$extName);
            $id = $this->imageLib->imageSaveInfo($params);
            $product->image_id = $id;
        }

        $product->save(); 

        $tobeUpdated = array();
        $tobeAdded = array();
        $tobeUpdatedKey = array();
        $allArray = array();
        foreach( $product->misc as $pm ){
            $allArray[] = $pm["id"];
        }
        foreach( $request->miscs as $misc ){
            if($misc['id'] != null){
                $tobeUpdated[] = $misc;
                $tobeUpdatedKey[] = $misc['id'];
            }
            else
                $tobeAdded[] = $misc;
        }

        $tobeDeleted = array_diff($allArray, $tobeUpdatedKey);
        if(count($tobeUpdated) > 0){
            foreach($tobeUpdated as $tbupdated){
                $miscUpdate = Misc::findOrFail($tbupdated['id']);
                $miscUpdate->wholesale_price = $tbupdated['wholesale_price'];
                $miscUpdate->retail_price = $tbupdated['wholesale_price'] * 2;
                $miscUpdate->size_id = $tbupdated['size_id'];
                $miscUpdate->quantity = $tbupdated['quantity'];
                $miscUpdate->sku = $tbupdated['sku'];
                $miscUpdate->updated_at = Carbon::now();
                $miscUpdate->save();
            }
        }

        if(count($tobeDeleted) > 0){
            Misc::destroy($tobeDeleted);
        }

        if(count($tobeAdded) > 0){
            foreach($tobeAdded as $added){
                $addedRec = new Misc;
                $addedRec->price = $added['price'];
                $addedRec->product_id = $id;
                $addedRec->size_id = $added['size_id'];
                $addedRec->quantity = $added['quantity'];
                $addedRec->sku = $added['sku'];
                $addedRec->save();
            }
        }



        return response()->json(array('tobeUpdated' => $tobeUpdated,'tobeAdded' => $tobeAdded, 'allArray' => $allArray, 'tobeDeleted' => $tobeDeleted));
        
    }

    public function dataPagination($model,$data){
        $dataPagination = array(
               "total" => $model->total(),
               "per_page" => $model->perPage(),
               "current_page" => $model->currentPage(),
               "last_page" => $model->lastPage(),
               "first_page_url" => $model->firstItem(),
               "last_page_url" => $model->lastPage(),
               "next_page_url" => $model->nextPageUrl(),
               "prev_page_url" => $model->previousPageUrl(),
               "path" => $model->total(),
               "from" => $model->firstItem(),
               "to"=> $model->lastItem(),
               "data" => $data,
            );
        return response()->json($dataPagination);
    }
}
