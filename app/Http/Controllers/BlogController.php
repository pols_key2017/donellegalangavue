<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class BlogController extends Controller
{

	public function index(){

		return View::make('frontend.blog.blog');
	}

    public function single($id = null){
    	$blogs = \App\Blog::findOrFail($id);
    	return View::make('frontend.blog.blog',['blog' => $blogs]);
    }
}
