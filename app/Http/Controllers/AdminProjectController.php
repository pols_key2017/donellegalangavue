<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Library\Imagelib;
use Carbon\Carbon;
use \App\Project;
use \App\Tag;
use Auth;
use View;

class AdminProjectController extends Controller
{
   protected $imageLib;
 
    public function __construct(Imagelib $Imagelib){
        $this->imageLib = $Imagelib;
    }

    public function index(){
   	
      $projects = \App\Project::orderBy('created_at', 'asc')->paginate(10);
     	foreach( $projects as $key => $project ){
              
          $data[] = array(
              'id' => $project->id,
              'name' => $project->name,
              'link' => $project->link,
              'tags' => \App\Project::find($project->id)->tags()->get(),
              'created_at' => !empty($project->created_at) ? Carbon::parse($project->created_at)->format('m/d/y') : '',
              'updated_at' => !empty($project->updated_at) ? Carbon::parse($project->updated_at)->format('m/d/y') : '',
              'project_image' => '', //=$project->image->id
              'image' => \App\Project::findOrFail($project->id)->image,
              );
      }
      $path = config('image.projectImgPath');
     $dataPagination = array(
         "total" => $projects->total(),
         "per_page" => $projects->perPage(),
         "current_page" => $projects->currentPage(),
         "last_page" => $projects->lastPage(),
         "first_page_url" => $projects->firstItem(),
         "last_page_url" => $projects->lastPage(),
         "next_page_url" => $projects->nextPageUrl(),
         "prev_page_url" => $projects->previousPageUrl(),
         "path" => $projects->total(),
         "from" => $projects->firstItem(),
         "to"=> $projects->lastItem(),
         "path" => $path[0],
         "data" => $data,
      );

     return response()->json($dataPagination);
   }

   public function single($id = null){
      if( $id > 0 ){
         $tags = \App\Project::find($id)->tags()->get();
         $projectTags = array();
         foreach($tags as $tag){
            $projectTags[] = $tag->id;
         }
         $project = \App\Project::find($id);
         $projectImage = \App\Project::find($id)->image()->first();
         $imagePath = config('image.projectImgPath');
        return response()->json(array(
                'project' => $project,
                'projectImage' => !empty($projectImage) ? $projectImage : '',
                'imagePath' => $imagePath[0],
                'tagsSelected' => $projectTags,
                'tags' => \App\Tag::all()
                ));
      }
   }

   public function store(Request $request){
        $proj =  new \App\Project;
        $proj->name = $request->input('title');
        $proj->link = $request->input('link');

        $proj->created_at = Carbon::now();
        $fileName = strip_tags($request->input('fileName'));
        $id_rand = mt_rand(10,100);
        $fileNameEnc = $this->imageLib->encode($id_rand,$fileName);

        $projectImagePath = config('image.projectImgPath');

        $dataArray = array(
              'cropper_data' => $request->input('cropData'),
              'photoUrl' => $request->input('photo_data_url'),
              'imagePath' => $projectImagePath[1],
              'fileName' => $request->input('fileName'),
              'id' => mt_rand(10,100)
          );


        if( count($request->input('photo_data_url')) > 0 ){
            $file = $this->imageLib->processImage($dataArray);
            $params = array(
                'encodedName' => $file['fileName'],
                'portion' => 'project',
                'extensionName' => $file['extensionName']
            );
            //$id = $this->imageSaveInfo($blog,$fileNameEnc,$id_rand,$extName);
            $id = $this->imageLib->imageSaveInfo($params);
            $proj->image_id = $id;
            $proj->save(); 
        }
       

        if( count($request->input('tags')) > 0) 
            $proj->tags()->attach($request->input('tags')); 

        return response()->json(array(
            'returned_data' => $id
            ));

    }

   public function update(Request $request, $id){
        $project = \App\Project::find($id);
        $project->link = $request->input('link');
        $project->name = $request->input('title');

        if( count($request->input('tags')) > 0) // sync removes the old data on the pivot table
            $project->tags()->sync($request->input('tags')); // you can only use sync if its not NULL

        $fileName = strip_tags($request->input('fileName'));
        $fileNameEnc = $this->imageLib->encode($id,$fileName);

        $projectImagePath = config('image.projectImgPath');
        $dataArray = array(
                'cropper_data' => $request->input('cropData'),
                'photoUrl' => $request->input('photo_data_url'),
                'imagePath' => $projectImagePath[1],
                'fileName' => $request->input('fileName'),
                'id' => $id
            );
        $project->save(); 

        
        if( count($request->input('photo_data_url')) > 0 ){
          $extName = $this->imageLib->processImage($dataArray); //returns array
          $imageData = array(
              'encodedFilename' => $fileNameEnc,
              'extensionName' => $extName['extensionName'],
              'id' => $id,
              'class' => array(
                  'name' => 'project',
                  'classModel' => $project
              )
          );
          $this->imageLib->imageSaveInfo($imageData);
        }
        

        // TODO
        
        // return response()->json(array(
        //     'returned_data' => $project->image->id
        //     ));
   }

   public function imageSaveInfo($project,$fileNameEnc,$id,$extName = null){
        $img = $project->image();
        if( $img->get()->count() <= 0 ){
            $params['name'] = $fileNameEnc;
            $params['updated_at'] = Carbon::now();
            $params['portion'] = 'featured';
            $params['ext_name'] = $extName;
            $image = new \App\Image($params);
            $img->save($image);
        }else{
            $img = $project->image;
            $images = \App\Image::find($img->id);
            $images->updated_at = Carbon::now();
            $images->name = $fileNameEnc;
            $images->ext_name = $extName;
            $images->save();
            
        }
        
    }
}
