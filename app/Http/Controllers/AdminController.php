<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Admin as Admin; // Add model This is the correct way in version 5.3
use Session;
use Input;
use Redirect;
use Form;
use View;


class AdminController extends Controller
{



	public function login(){
		return View::make("backend.login");
	}
    
    public function attempt(){
    	 $rules = array(                     // just a normal required validation
	        'email'            => 'required|email',     // required and must be unique in the ducks table
	        'password'         => 'required'           // required and has to match the password field
	    );

    	$validator = Validator::make(Input::all(), $rules);
    	if ($validator->fails()) {
    		// get the error messages from the validator
        	$messages = $validator->messages();

	        // redirect our user back to the form with the errors from the validator
	        return Redirect::to('admin/login')
	        ->withInput()
            ->withErrors($validator);
    	} else{
    		$auth = Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')]);
    		if( $auth ){
    			$session = Session::put('admin_user',$auth);

    			if( $this->isAdminLogged() ){
    				return Redirect::to('admin/blog');
    			}
    		}else{
    			echo "Wrong Input.,";
    		} 
    	}
    }

    public function isAdminLogged(){

    	if( Session::has('admin_user') ){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function dashboard(){
        return View::make('backend.dashboard');
    }

    public function logout(){
        Admin::logout();
        return Redirect::to('admin/login');
    }
}
