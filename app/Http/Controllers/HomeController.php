<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['blogs'] = \App\Blog::where('status','=','publish')->limit(3)->orderBy('created_at', 'asc')->get();
        $data['projects'] = \App\Project::get();
        $data['tags'] = \App\Tag::get();
        return View::make('frontend.blog.index',['datas' => $data]);
    }

    public function project(){
        $projects = \App\Project::get();
        foreach($projects as $project ){
            echo $project->name.'<br/>';
            echo $project->image->name;
            foreach($project->tags as $tags){
                echo '--'.$tags->tags."<br/>";
            }
        }
    }
}
