<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order; 
use App\Misc;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminOrderController extends Controller
{
    public function addOrder(Request $request){
        $latestOrder = Order::orderBy('created_at','DESC')->first();
        $order_number = str_pad($latestOrder->id + 1, 8, "0", STR_PAD_LEFT);
    	foreach($request->input('product') as $product){
            
    		$order =  new Order;
    		$order->product_id = (int)$product['id'];
    		$order->size_id = (int)$product['size_id'];
    		$order->quantity = (int)$product['quantity'];
    		$order->selling_price = (float)$product['origPrice'];
            $order->order_number = $order_number;
    		$order->total = (float)($product['price'] * $product['quantity']);
    		if($order->save()){
                $Misc = Misc::findOrFail( (int) $product['misc_id']);
                $Misc->quantity = $Misc->quantity - $product['quantity']; // removes a quantity on misc
                $Misc->save();
            }

    	}
    	return response()->json(array(
    			'message' => 'Information has been Saved!',
                'datas' => $request->input('product')
    		));
    	
    }

    public function dateInterval($startDate, $endDate){

        /*User::with(['messages' => function($query) {
            $query->select('messages.user_id');
            $query->groupBy('user_id');
        }])->get();*/

        $orders = Order::with('product','size')
                    ->whereBetween('created_at',[$startDate, $endDate])
                    //->groupBy('order_number')
                    //->selectRaw('order_number, sum(total) as sum_total, sum(quantity) as sum_quantity')
                    ->get();

        $store = array();

        foreach($orders as $value){
            $store[] = array('name' => $value->product->product_name, 'quantity' => $value->quantity);
        }

        $data = array();
        $saver = array();
        $i = 0;
        foreach($store as $stor){
            if($i == 0)
                $saver[] = array('Product Name','Quantity');
            if(!in_array($stor['name'], $data)){
                $data[] = $stor['name'];
                $saver[] = array($stor['name'],$stor['quantity']);
            }else{
                foreach($saver as $k => $s){
                    if($s[0] == $stor['name']){
                        $saver[$k][1] += $stor['quantity'];
                    }
                }
            }
            $i++;
        }

        return response()->json(array(
                'orders' => $orders,
                'graphdata' => $saver
            )); 
    }
}
