<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Library\Imagelib;
use \Hash;

use ImageCL;

use \App\User; 
use \App\Userinfo;
use \App\Image as Image;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminUserController extends Controller
{
    protected $imageLib;
 
    public function __construct(Imagelib $Imagelib){
        $this->imageLib = $Imagelib;
    }

    public function userRoles(){
        return response()->json(array('roles' => Role::all()));
    }

    public function index(){
        $users = \App\User::whereNotIn('id',[1])->paginate(10);
        $data = array();
        foreach($users as $key => $user ){
            $imageCheck = ($user->userList->count() > 0 ) ? $user->userList->image_id : 57;
            $image = \App\Image::findOrFail($imageCheck);
            $data[] = array(
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'profession' => $user->userList->profession,
                        'street' => $user->userList->street,
                        'phone' => $user->userList->phone,
                        'city' => $user->userList->city,
                        'state' => $user->userList->state,
                        'country' => $user->userList->country,
                        'score' => $user->userList->score,
                        'roles' => $user->role_id,
                        'profileImg' => $image->name.".".$image->ext_name,
            );
        }
        $path = config('image.profileImgPath');
        $dataPagination = array(
               "total" => $users->total(),
               "per_page" => $users->perPage(),
               "current_page" => $users->currentPage(),
               "last_page" => $users->lastPage(),
               "first_page_url" => $users->firstItem(),
               "last_page_url" => $users->lastPage(),
               "next_page_url" => $users->nextPageUrl(),
               "prev_page_url" => $users->previousPageUrl(),
               "path" => $path[1],
               "from" => $users->firstItem(),
               "to"=> $users->lastItem(),
               "profilePath" => $path[0],
               "data" => $data,
            );

        return response()->json($dataPagination);
    }

	public function store(Request $request){

        $user                   =  new \App\User;
        $user->role_id          = (int)$request->input('roles');
        $user->name             = $request->input('firstname')." ".$request->input('lastname');
        $user->email            = $request->input('email');
        $user->password         = Hash::make($request->input('password'));
        $user->created_at       = Carbon::now();
        $user->updated_at       = Carbon::now();
        $user->save();
		$id = $user->id;

        // saves images info
        $fileName = strip_tags($request->input('fileName'));
        $id_rand = mt_rand(10,100);
        $fileNameEnc = $this->imageLib->encode($id_rand,$fileName);

        $dataArray = array(
                        'cropper_data' => $request->input('cropData'),
                        'photoUrl' => $request->input('photo_data_url')
                    );

        if( $request->input('photo_data_url') != NULL ){
            $extName = $this->imageProcess($dataArray,$fileNameEnc);
            $img_id = $this->imageSaveInfo($user,$fileNameEnc,$id_rand,$extName);
         }else{
            $img_id = 1; // default to 1 -> add this in live, create a default image also
         }

         $userinfo               = new \App\Userinfo;
         $userinfo->user_id      = $id;
         $userinfo->image_id     = $img_id; 
         $userinfo->firstname    = $request->input('firstname');
         $userinfo->lastname     = $request->input('lastname');
         $userinfo->profession   = $request->input('profession');
         $userinfo->street       = $request->input('street');
         $userinfo->phone        = $request->input('phone');
         $userinfo->city         = $request->input('city');
         $userinfo->state        = $request->input('state');
         $userinfo->country      = $request->input('country');
         $userinfo->score        = 0;
         $userinfo->created_at   = Carbon::now();
         $userinfo->updated_at   = Carbon::now();
         $userinfo->save();

         

        return response()->json(array('data'=> true ));


	}

    public function single($id = null){
        if( $id > 0 ){
            $user = \App\User::where( 'id', (int) $id)->first();
            $userinfo = \App\User::findOrFail($id)->userList()->first();
            $imagePath = config('image.profileImgPath');
            return response()->json(array(
                'userinfo' => $userinfo,
                'user'=> $user,
                'imagePath' => $imagePath[0],
                'profileImage' =>  \App\Image::findOrFail($userinfo->image_id),
                'authInfo' => auth()->user()->getRoleNames(),
                'roles' => Role::all(),
                ));
        }
    }

    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    
    public function update(Request $request, $id){

        $user = \App\User::with('userList')->find($id);
        // OTHER WAY AROUND TO SAVE
        // $user->name = $request->input('firstname');
        // $user->save();
        // $user->userList->update([
        //         'firstname' => $request->input('firstname')
        //     ]);
        $user->name                     = $request->input('firstname');
        $user->role_id                  = (int) $request->input('roleId');
        $user->email                    = $request->input('email');
        $user->password                 = Hash::make($request->input('password'));
        $user->userList->firstname      = $request->input('firstname');
        $user->userList->lastname       = $request->input('lastname');
        $user->userList->profession     = $request->input('profession');
        $user->userList->street         = $request->input('street');
        $user->userList->phone          = $request->input('phone');
        $user->userList->city           = $request->input('city');
        $user->userList->state          = $request->input('state');
        $user->userList->country        = $request->input('country');
        $user->userList->score          = 4; // this is to be updated
        $user->userList->updated_at     = Carbon::now();
        $user->push();    
        
        $fileName = strip_tags($request->input('fileName'));
        $fileNameEnc = $this->imageLib->encode($id,$fileName);

        $dataArray = array(
                'cropper_data' => $request->input('cropData'),
                'photoUrl' => $request->input('photo_data_url')
            );
        if( strlen($request->input('photo_data_url')) > 0 ){
            $extName = $this->imageProcess($dataArray,$fileNameEnc);

            $imageId = $this->imageSaveInfo($user,$fileNameEnc,$id,$extName);
            $user->userList->image_id = $imageId;
            $user->push();

            return $imageId;
        }

        //return $fileNameEnc;
    }

    public function imageSaveInfo($user,$fileNameEnc,$id,$extName = null){
            $image = new \App\Image;
            $image->blog_id = 0;
            $image->portion = 'avatar';
            $image->name =  $fileNameEnc;
            $image->ext_name = $extName;
            $image->created_at = Carbon::now();
            $image->updated_at = Carbon::now();
            $image->save();
            return $image->id;
    }

    public function imageProcess( $array,$fileNameEnc ){
        // process image

        $profileImagePath = config('image.profileImgPath');
        $imgExt = '';
        if(strlen($array['photoUrl']) > 0){
            $img = ImageCL::make($array['photoUrl']);

            $imgExt = $this->imageLib->getFileExtension($img->mime());


                if( count($array['cropper_data']) > 0 ){
                    $img->crop(intval($array['cropper_data']['width']), intval($array['cropper_data']['height']), intval($array['cropper_data']['x']), intval($array['cropper_data']['y']))
                    ->resize(256, 256,
                        function ($constraint) {
                            $constraint->aspectRatio();
                        })
                    ->save($profileImagePath[1] . $fileNameEnc . '@avatar.' . $imgExt);
                    
                }

            $img->resize(520, 400,
                function ($constraint) {
                    $constraint->aspectRatio();
                })->save($profileImagePath[1] . $fileNameEnc . '.' . $imgExt);
            $img->destroy();
        }

        return $imgExt;
    }

    public function checkEmailExists($email){
    	$user = \App\User::where('email',$email)->exists();
    	if($user) // if exists
    		$checker = false;
    	else
    		$checker = true;
    	return response()->json(array('data'=> $checker ));
    }
}
