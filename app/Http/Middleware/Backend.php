<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Router;
use App\Models\Admin as Admin;
use Session;
use Request;
use Redirect;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class Backend {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{		
			$is_admin = (Session::get('admin_user')) ? true :  false;
			if( $is_admin ) {
				
				/*if(Auth::user()->role_id != 1) {
					//Admin::logout();
					return Redirect::to('/');			
				} */
			} else {
				Session::put('url',Request::url());
				return Redirect::to('admin/login');
			}

		return $next($request);
	}

}
