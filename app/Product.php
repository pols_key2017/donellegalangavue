<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{


    public function misc(){
        return $this->hasMany('App\Misc')->with('size');
    }

    public function mistCount(){
        return $this->hasMany('App\Misc')->count();
    }

    public function brand(){
    	return $this->belongsTo('App\Brand');
    }

    public function category(){
    	return $this->belongsTo('App\ProductCategory','product_category_id');
    }

    public function image(){
    	return $this->belongsTo('App\Image');
    }

    public function productImage(){
    	return $this->belongsTo('App\Image');
    }

}
