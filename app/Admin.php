<?php
namespace App;
use DB;
use Hash;
use Input;
use Session;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
class Admin extends Model{

	use HasRoles;

	protected $table = 'users';

	protected $guard_name = 'web'; 


	public static function check() {
		
		(Session::get('admin_user')) ? $check = true : $check = false;

		return $check;
	}

	public static function user() {
		
		if(Session::get('admin_user')) {
			$exists = DB::select("SELECT * FROM users WHERE id = ".Session::get('admin_user')[0]->id." AND role_id = 1 AND status = 1");
			if($exists){
				Session::put('admin_user',$exists);
			}
		}
		return Session::get('admin_user');
	}

	public static function user_checker(){
		return true;
	}

	public static function attempt() {
	
		$username = Input::get("username");
		$password = Input::get("password");
	
		$check = DB::select("SELECT * FROM users WHERE email = '$username' AND status = 1");


		if($check){
			 $hashedPassword = $check[0]->password;
			if (Hash::check($password, $hashedPassword))

			{
			   Session::put('admin_user',$check);
			   return 'true';
			}

		}

		return Null;
	}
	public static function login($id) {
		$check = DB::select("SELECT * FROM users WHERE id = '$id' AND role_id =1 AND status = 1");

		if($check){
		   Session::put('admin_user',$check);
		   return true;
		} else
			return false;
	}
	
	public static function logout() {
		Session::forget('admin_user');
	}

}
