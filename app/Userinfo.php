<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    protected $table = 'user_info';

    protected $fillable = array('image_id','firstname','lastname','profession','street','phone','city','state','country','score');

    protected $attributes = ['profession' => 'none'];

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function profileImg(){
        return $this->belongsTo('\App\Image');
    }
}
