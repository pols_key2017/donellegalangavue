<?php
namespace App\Library;

use ImageCL;
use Carbon\Carbon;
use \App\Image;
 
class Imagelib {

   public $allowed_types = [
	    'image/bmp',
	    'image/png',
	    'image/jpg',
	    'image/gif',
	    'image/jpeg'
   ];
 
   public function encode($blogID,$filename) {
        return md5($filename.$blogID);
   }

   /**
     * Return file extension by mime type
     *
     * @param string $type
     * @return boolean|string
     */
    public function getFileExtension($type){
        if (!$this->isAllowedType($type)) {
            return FALSE;
        }
        $arr = explode('/', $type);
        return $arr[1];
    }

    /**
     * Checks if the mime type is allowed
     *
     * @param string $type
     * @return boolean
     */
    public function isAllowedType($type){
        return in_array($type, $this->allowed_types);
    }

    public function processImage($array){
        $imgExt = '';
        if(strlen($array['photoUrl']) > 0){
            $img = ImageCL::make($array['photoUrl']);
            $fileName = strip_tags($array['fileName']);
            $fileNameEnc = $this->encode($array['id'],$fileName);

            $imgExt = $this->getFileExtension($img->mime());
             if( count($array['cropper_data']) > 0 ){
                $img->crop(intval($array['cropper_data']['width']), intval($array['cropper_data']['height']), intval($array['cropper_data']['x']), intval($array['cropper_data']['y']))
                ->resize(256, 256,
                    function ($constraint) {
                        $constraint->aspectRatio();
                    })
                ->save($array['imagePath'] . $fileNameEnc . '@avatar.' . $imgExt);
                
            }

            $img->resize(520, 400,
                function ($constraint) {
                    $constraint->aspectRatio();
                })->save($array['imagePath'] . $fileNameEnc . '.' . $imgExt);
            $img->destroy();
        }
        return array(
            'extensionName' => $imgExt,
            'fileName' => $fileNameEnc
        );
    }

    public function imageSaveInfo($params){
        if(!empty($params['id'])){ // TODO
            $id = '';
            if($params['class']['name'] == 'project'){
                $project = $params['class']['classModel'];
                $projectImage = $project->image();
                if( $project->image()->get()->count() <= 0 ){// if no data
                    echo "no data";
                }else{
                    $img = $project->image;
                    $images = \App\Image::find($img->id);
                    $images->updated_at = Carbon::now();
                    $images->portion = 'project';
                    $images->name = $params['encodedFilename'];
                    $images->blog_id = 0;
                    $images->ext_name = $params['extensionName'];
                    $images->save();
                }
            }
        }else{
            $image = new \App\Image;
            $image->name = $params['encodedName'];
            $image->created_at = Carbon::now();
            $image->portion = 'project';
            $image->ext_name = $params['extensionName'];
            $image->blog_id = 0;
            $image->save();
            $id = $image->id;
        }
        return $id;
    }
 
}