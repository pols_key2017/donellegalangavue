<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = array('id','blog_id','name','created_at','ext_name');

    public function blog(){
    	return $this->belongsTo('\App\Blog');
    }

    public function profile(){
    	return $this->belongsTo('\App\Userinfo');
    }

    public function getImagePathAttribute(){
    	return "{$this->name}.{$this->ext_name}";
    }
}
