<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\image as Image;

class Project extends Model
{
    public function tags(){
    	return $this->belongsToMany('\App\Tag');
    }

    public function image(){
    	return $this->belongsTo('\App\Image');
    }
}
