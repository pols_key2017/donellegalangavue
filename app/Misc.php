<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Misc extends Model
{

    public function product(){
    	return $this->belongsTo('\App\Product');
    }

    public function size(){
    	return $this->belongsTo('\App\Size');
    }

}
