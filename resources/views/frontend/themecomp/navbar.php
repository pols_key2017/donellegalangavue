<!-- Navbar -->
<div class="s-header__navbar">
    <div class="s-header__container">
        <div class="s-header__navbar-row">
            <div class="s-header__navbar-row-col">
                <!-- Logo -->
                <div class="s-header__logo">
                    <a href="index.html" class="s-header__logo-link">
                        <img class="s-header__logo-img s-header__logo-img-default" src="img/logo.png" alt="Megakit Logo">
                        {!!Html::image('img/logo-dark.png','Donelle Galanga Logo',array('class' => 's-header__logo-img s-header__logo-img-shrink'))!!}
                    </a>
                </div>
                <!-- End Logo -->
            </div>
            <div class="s-header__navbar-row-col">
                <!-- Trigger -->
                <a href="javascript:void(0);" class="s-header__trigger js__trigger">
                    <span class="s-header__trigger-icon"></span>
                    <svg x="0rem" y="0rem" width="3.125rem" height="3.125rem" viewbox="0 0 54 54">
                        <circle fill="transparent" stroke="#fff" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
                    </svg>
                </a>
                <!-- End Trigger -->
            </div>
        </div>
    </div>
</div>

<!-- End Navbar -->