@extends('frontend.layout.master')
@section('content')
<!-- FA Questions Text -->
<!-- Masonry -->
<div class="g-hor-divider__dashed--sky-light">
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="row">
            <div class="col-md-9 col-md-push-3 g-margin-t-0--xs g-margin-t-70--lg g-margin-b-60--xs g-margin-b-0--lg">
                <div class="g-margin-b-40--xs">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">
                    	@foreach($blog->tags()->get() as $tags)
                    		{{ @$tags->tags }}
                    	@endforeach
                    	</p>
                    <h2 class="g-font-size-32--xs g-font-size-36--sm g-margin-b-30--xs">{{ @$blog->title }}</h2>
                    {!! $blog->content !!}
                </div>
            </div>
            <div class="col-md-3 col-md-pull-8">
                <!-- Masonry Grid -->
                <div class="row g-row-col--5 js__masonry">
                    <div class="col-xs-6 col-sm-6 col-md-1 js__masonry-sizer"></div>
                    <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs js__masonry-item">
                    	{!!Html::image('images/blog/'.@$blog->featuredImage->name.'.'.@$blog->featuredImage->ext_name,'',array('class' => 'img-responsive g-width-100-percent--xs'))!!}
                    </div>
                </div>
                <!-- End Masonry Grid -->
            </div>
        </div>
    </div>
</div>
<!-- End Masonry -->
<!-- End FA Questions Text -->
@stop