<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Pols Webworks | </title>
    <link href="/css/all.css" rel="stylesheet">
    <!-- Bootstrap -->
    <!-- <link href="{{URL::asset('vendor/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">-->
    <!-- Font Awesome -->
    <!-- <link href="{{URL::asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">-->
    <!-- NProgress -->
    <!-- <link href="{{URL::asset('vendor/nprogress/nprogress.css')}}" rel="stylesheet">-->
    <!-- Animate.css -->
    <!-- <link href="{{URL::asset('vendor/animate.css/animate.min.css')}}" rel="stylesheet">-->

    <!-- Custom Theme Style -->
    <!--<link href="{{URL::asset('build/css/custom.min.css')}}" rel="stylesheet"> -->
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>        
                @endforeach
            </div>
          @endif

         <?php //var_dump($errors->all()); ?>
            <form action="{{ URL::to('admin/attempt')}}" method="POST" id="formLogin">
              <h1>Login Form</h1>
              <div>
               {{Form::text('email','',array('id'=>'','class'=>'form-control','placeholder' => 'Email'))}}
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
              </div>
              <div>
                <button class="btn btn-default submit" type="submit">Log in</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> PolsWebworks!</h1>
                  <p>©2016 All Rights Reserved. </p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form >
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button class="btn btn-default submit" href="index.html">Submit</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
<!--<script src="{{asset('js/jquery.min.js')}}"></script> -->
<script src="/js/all.js" type="text/javascript"></script>
<script>
function Admin_Login() {
    this.construct();
};

Admin_Login.prototype = {

    dialog : false,

    construct : function() {
        this.property_id = 'test';
        this.init_events();
    },

    test_init : function() {
      $('#formLogin').submit();
    },  

    init_events : function() {

        var $self = this;

        $( '.submit' ).click(function(){
          $self.test_init();
        });

    }

};

var $Admin_Login;

jQuery( document ).ready(
    function() {
        $Admin_Login = new Admin_Login();
    }
);
</script>
<!-- Scripts -->
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
