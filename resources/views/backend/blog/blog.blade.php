@extends('backend.layout.master')
@section('content')
<!-- page content -->
<!-- This is where the router vue propagates -->
<div class="right_col" role="main">
  <div class="">
    <!-- search vue resides -->
    <search></search>
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <router-view></router-view>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal (Pop up when detail button clicked) -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel" >Title</h4>
                </div>
                <div class="modal-body">
                    <form name="frmBlog" class="form-horizontal" novalidate="">

                        <div class="form-group error">
                            <label for="inputEmail3" class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control has-error" id="title" placeholder="Title" name="title" value="" 
                                >
                                <span class="help-inline" >Title field is required</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-sm-3 control-label">Content</label>
                            <div class="col-sm-9">
                                <textarea name="content" class="form-control" id="content" cols="30" rows="10" placeholder="Content" ></textarea>
                                <span class="help-inline" >Content field is required</span>
                            </div>
                        </div>

                        <div class="form-group error">
                            <label for="tags" class="col-sm-3 control-label">Tags</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control has-error" id="tags" name="tags" placeholder="Tags" value=""  >
                                <span class="help-inline" >Tags is required</span>
                            </div>
                        </div>

                        <div class="form-group error">
                            <label for="tags" class="col-sm-3 control-label">Action</label>
                            <div class="col-sm-9">
                                  <select class="form-control has-error" name="status" id="status">
                                    <option value=""></option>
                                  </select>
                                <span class="help-inline" >Status is required</span>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" >Save changes</button>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /page content -->


@stop