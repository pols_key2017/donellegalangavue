import VueRouter from 'vue-router';

const Blog = require('./views/blog.vue')
const Project = require('./views/project.vue')
const User = require('./views/user.vue')
const Supplier = require('./views/supplier.vue')
const Category = require('./views/category.vue')
const Brand = require('./views/brand.vue')
const Stock = require('./views/stock.vue')
const BuyProduct = require('./views/buyproduct.vue')
const Order = require('./views/order.vue')

const routes = [
  { path: '/', component: Blog},
  { path: '/blog', component: Blog},
  { path: '/project', component: Project},
  { path: '/user', component: User},
  { path: '/supplier', component: Supplier},
  { path: '/categories', component: Category},
  { path: '/brand', component: Brand},
  { path: '/stocks', component: Stock},
  { path: '/buyproduct', component: BuyProduct},
  { path: '/order', component: Order},

]

export default new VueRouter({
	routes,
	linkActiveClass: 'active'
})