export default class Spatie {
	
	constructor(user){
		this.user = user;
	}

	isSuperAdmin(){
		return this.user == 'super admin';
	}

	isStaff(){
		return this.user == 'staff';
	}
}