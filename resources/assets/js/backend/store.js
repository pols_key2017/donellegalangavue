
module.exports = {
	state: {	
		blogs: {},
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
    	productCart: {
    		triggerModal: false,
    		products: [],
    		selectedProducts: [],
    	},
    	productSave: [],
        imgSrc: '',
        fileName: '',
        projects: {},
        singleproject: {
        	name: '',
        },
        user: {},
        suppliers: {},
        products: {},
        profilePath: '',
        categories: {},
        brands: {},
        productsearched:{},
        orders: {},
        graphData: [],
    },
        
	mutations: {
		renderBlog(state, data){
			state.blogs = data.data;
			state.pagination = data;
		},
		saveImgSrc(state, data){
			state.imgSrc = data.imgSrc;
			state.fileName = data.fileName;
		},
		renderProject(state, data){
			state.projects = data.data;
			state.pagination = data;
		},
		renderUser(state, data){
			state.user = data.data;
			state.profilePath = data.profilePath
		},
		renderSingleProject(state, data){
			state.singleproject = data;
		},
		renderSupplier(state, data){
			state.suppliers = data.data;
			state.pagination = data;
		},
		renderProducts(state, data){
			state.products = data.data;
			state.pagination = data;
		},
		renderCategory(state, data){
			state.categories = data.data;
			state.pagination = data;
		},
		renderBrand(state, data){
			state.brands = data.data;
			state.pagination = data;
		},
		updateCartData(state, data){
			
			if(data.action == 'add'){
				var variable = data.data
				var products = []
				var misc = []
				console.log(variable);
				$.each(data.data.misc, (key, value) => {
					misc.push(
						{
							id:key,
							misc_id: value.id,
							price: value.retail_price,
							size_id: value.size_id,
							quantity: value.quantity,
							sku: value.sku,
							quantityChanger: value.quantity,
							size: value.size,
						}
					)
				})
				state.productSave.push({id:variable.id,productname: variable.productName,price: 0, lastprice: 0, misc: misc, total: 0, origPrice: 0, quantityHolder: 0,currentMisc: 0})
			}else{
				
			}
			//
		},
		renderSearch(state, params ){
			switch(params.filter){
				case "productcode":
					axios.get(params.url)
		              .then( response => {
		              		state.productsearched = response.data
		              })
				break;
			}
		},
		cartPopulate(state, data){
			if(data.action){
				state.productCart.triggerModal = true;
			}else{
				state.productCart.triggerModal = false;
			}
			
		},
		ORDERPOPULATE(state, data){
			state.orders = data.orders
			state.graphData = data.graphdata
		}
		
	},
	actions: {
		populateBlog(context,page){
			var url = '/admin/populateblog?page='+page
            axios.get(url)
              .then( response => {
                context.commit('renderBlog',response.data);
              })
			
		},
		showCart(context,data){
			context.commit('cartPopulate', data);
		},
		populateCategories(context,page){
			var url = '/admin/category?page='+page
            axios.get(url)
              .then( response => {
                context.commit('renderCategory',response.data);
              })
		},
		populateBrands(context,page){
			var url = '/admin/brand?page='+page
            axios.get(url)
              .then( response => {
                context.commit('renderBrand',response.data);
              })
		},
		populateProject(context,page){
			var url = '/admin/project?page='+page;
			axios.get(url)
				.then( response => {
					context.commit('renderProject',response.data);
				})
		},
		populateUser(context,page){
			axios.get('/admin/user?page='+page)
				.then( response => {
					context.commit('renderUser',response.data);
				})
		},
		populateSupplier(context,page){
			axios.get('/admin/supplier?page='+page)
				.then( response => {
					context.commit('renderSupplier', response.data);
				})
		},
		populateStock(contect,page){
			axios.get('/admin/product?page='+page)
				.then( response => {
					contect.commit('renderProducts', response.data);
				})
		},
		trigSaveImgSrc(context,data){
			context.commit('saveImgSrc',data);
		},
		singleProject(context,id){
			var url = '/admin/project/'+id;
			axios.get(url)
				.then( response => {
					context.commit('renderSingleProject',response.data);
				})
		},
		searchData(context,parameters){
			var url = '';
			switch(parameters.action){
				case "searchProduct":
					url = '/admin/productsearch/'+parameters.filter+'/'+parameters.name+'?page='+parameters.page;
					filter = parameters.filter;
				break;

				case "searchAllProduct": // search all Product
					url = '/admin/productsearch/'+parameters.filter+'?page='+parameters.page;
					filter = parameters.filter;
				break;

				default: 
					

			}
			context.commit('renderSearch',{
				url : url,
				filter : filter,
			});
		},
		updateCart(context, data){
			context.commit('updateCartData',data);
		},
		orderPopulate(context,data){
			console.log(data);
			axios.get('/admin/order/date/'+data.startDate+'/'+data.endDate)
				.then( response => {
					context.commit('ORDERPOPULATE', response.data);
				})
		}
	},
	getters: {
		blogs: state => state.blogs,
		projects: state => state.projects,
		pagination: state => state.pagination,
		suppliers: state => state.suppliers,
		imgSrc: state => state.imgSrc,
		fileName: state => state.fileName,
		singleproject: state => state.singleproject,
		user: state => state.user,
		profilePath: state => state.profilePath,
		categories: state => state.categories,
		brands: state => state.brands,
		products: state => state.products,
		productsearched: state => state.productsearched,
		productCart: state => state.productCart,
		prodSample: state => state.prodSample,
		productSave: state => state.productSave,
		orders: state => state.orders,
		graphData: state => state.graphData,
	}
}