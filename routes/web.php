<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Models\Admin;


Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/test', 'AdminInventoryController@test');

// Admin

Route::get('admin/login', 'AdminController@login');


Route::get('admin/attempt', 'AdminController@attempt');
Route::post('admin/attempt', 'AdminController@attempt');



Route::group(array('before' => 'admin', 'prefix' => 'admin', 'middleware' => 'backend'), function() {

	// BLOG
	Route::get('bloginit','AdminBlogController@bloginit');

	Route::group(array('prefix'=>'blog'),function(){
		
		Route::post('/','AdminBlogController@store');
		Route::get('showalltags','AdminBlogController@populateTags');

		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminBlogController@single');
			Route::post('/','AdminBlogController@update');
		});

	});


	Route::group(array('prefix'=>'project'),function(){
		Route::get('/','AdminProjectController@index');
		Route::post('/','AdminProjectController@store');
		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminProjectController@single');
			Route::post('/','AdminProjectController@update');
		});
		
	});

	Route::group(array('prefix'=>'user'),function(){
		Route::get('/','AdminUserController@index');
		Route::post('/','AdminUserController@store');
		Route::get('roles','AdminUserController@userRoles');
		Route::get('check/{email}','AdminUserController@checkEmailExists');
		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminUserController@single');
			Route::post('/','AdminUserController@update');
		});


	});

	Route::group(array('prefix'=>'supplier'),function(){
		Route::post('/','AdminInventoryController@store');
		Route::get('/','AdminInventoryController@populateSupplier');
		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminInventoryController@single');
			Route::post('/','AdminInventoryController@update');
		});
	});

	Route::group(array('prefix'=>'category'),function(){
		Route::get('/','AdminInventoryController@populateCategories');
		Route::post('/','AdminInventoryController@addCategory');
		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminInventoryController@singleCategory');
			Route::post('/','AdminInventoryController@updateCategory');
		});
	});

	Route::group(array('prefix'=>'brand'),function(){
		Route::post('/','AdminInventoryController@addBrand');
		Route::get('/','AdminInventoryController@populateBrand');
		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminInventoryController@singleBrand');
			Route::post('/','AdminInventoryController@updateBrand');
		});
	});

	Route::group(array('prefix'=>'product'),function(){
		Route::get('/','AdminInventoryController@populateStock');
		Route::post('/','AdminInventoryController@addProduct');
		Route::group(array('prefix'=>'{id}'),function(){
			Route::get('/','AdminInventoryController@singleProduct');
			Route::post('/','AdminInventoryController@updateProduct');
		});
	});

	Route::group(array('prefix'=>'productsearch'),function(){
		Route::group(array('prefix'=>'productcode'),function(){
			Route::get('/','AdminInventoryController@searchAllProduct');
			Route::group(array('prefix'=>'{name}'),function(){
				Route::get('/','AdminInventoryController@searchProduct');
			});
		});
	});

	Route::group(array('prefix'=>'order'),function(){
		Route::post('/','AdminOrderController@addOrder');
		Route::group(array('prefix' => 'date'), function(){
			Route::get('{startdate}/{enddate}', 'AdminOrderController@dateInterval');
		});
	});



	
	Route::get('blog','AdminBlogController@index');
	Route::get('populateblog', 'AdminBlogController@populateBlog');
	Route::get('showproductattr','AdminInventoryController@showProductDefaultAttr');
	Route::post('blog','AdminBlogController@store');
	//Route::get('blog/{id}','AdminBlogController@single');
	//Route::post('blog/{id}','AdminBlogController@update');
	//Route::get('show/{id}','AdminBlogController@show');

	Route::delete('blog/{id}','AdminBlogController@destroy');
	//Route::delete('blog','AdminBlogController@index');
	//Route::resource('blog' , 'AdminBlogController', array('as'=>'blog') );

	Route::get('logout', 'AdminController@logout');
});


//for Frontend
//Route::get('blog','BlogController@index');
Route::group(array('prefix'=>'blog'),function(){
	Route::get('/','BlogController@single');
	Route::group(array('prefix'=>'{id}'),function(){
		Route::get('/','BlogController@single');
	});
});


// ARTISAN 
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    //$exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    //$exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    //$exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    //$exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    //$exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    //$exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/create_project_table', function() {
    //$exitCode = Artisan::call('make:migration', ['name' => 'create_project_table']);
    return '<h1>create_project_table</h1>';
});

Route::get('/checkurl', function() {
    $exitCode = URL::to('/images/blog').'/';
    return $exitCode;
});

Route::get('/portfolio', 'HomeController@project');



