"use strict";

let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
// FOR BACKEND
mix.styles([
       'resources/assets/css/backend/bootstrap/dist/css/bootstrap.min.css',
       'resources/assets/css/backend/nprogress/nprogress.css',
       'resources/assets/css/backend/fontawesome/css/font-awesome.min.css',
       'resources/assets/css/backend/animate.css/animate.min.css',
       'resources/assets/css/backend/iCheck/skins/flat/green.css',
       'resources/assets/css/build/css/custom.min.css',
       '~selectize/dist/css/selectize.bootstrap3.css'
  ],'public/css/framework-bundle.css') // for the framework
  .js([
      'resources/assets/js/backend/bundlejs/jquery/dist/jquery.min.js',
      'resources/assets/js/backend/bundlejs/bootstrap/dist/js/bootstrap.js',
      'resources/assets/js/backend/bundlejs/fastclick/lib/fastclick.js',
      'resources/assets/js/backend/bundlejs/nprogress/nprogress.js',
      'resources/assets/js/backend/bundlejs/iCheck/icheck.min.js',
      'resources/assets/js/build/js/custom.js',
    ],'public/js/framework-bundle.js')
  .less('resources/assets/less/backend.less', 'public/css/backend.css');
mix.autoload({
    jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"]
});
  mix.js('resources/assets/js/backend/backend.js','public/js/framework-app.js');

// FOR FRONTEND
mix.sass('resources/assets/scss/frontend/style.scss','public/css/frontend-framework-bundle.css');
mix.styles([
    'resources/assets/js/frontend/bootstrap/css/bootstrap.min.css',
    'resources/assets/css/frontend/animate.css',
    'resources/assets/js/frontend/themify/themify.css',
    'resources/assets/js/frontend/scrollbar/scrollbar.min.css',
    'resources/assets/js/frontend/magnific-popup/magnific-popup.css',
    'resources/assets/js/frontend/swiper/swiper.min.css',
    'resources/assets/js/frontend/cubeportfolio/css/cubeportfolio.min.css',
    'resources/assets/css/frontend/style.css',
    'resources/assets/css/frontend/global/global.css'
  ],'public/css/frontend.css');
  

  mix.browserSync({
    proxy: 'donellegalangavue.local'
});

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.ts(src, output); <-- Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
